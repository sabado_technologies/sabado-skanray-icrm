<?php 
header('Access-Control-Allow-Origin: *');

header('Access-Control-Allow-Headers: *');

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_api extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
		$this->load->model("Common_model");
		$this->load->model("Calendar_model");
		$this->load->model("ajax_model");
	}

	public function addVisit()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $_SESSION['company'] = $post_data['company_id'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $_SESSION['locationString'] = $post_data['locationString'];
        $_SESSION['role_id'] = $post_data['role_id'];
        $leads = $this->Calendar_model->getLeadDetails();
        $lead_arr = array();
        foreach ($leads as $key=>$lead) 
        {
            $lead_arr[$key]['lead_id'] = $lead['lead_id'];
            $lead_arr[$key]['name'] = "Lead ID - ".$lead['lead_number']." (".$lead['CustomerName'].")";
        }
        $data['leads'] = $lead_arr;
        $data['purpose'] = $this->Common_model->get_data('visit_purpose',array());
        $this->session->sess_destroy();
        echo json_encode($data); 

    }

    public function visitAdd()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $dataArr = array
                    (
                        'visit_id'   => '',
                        'lead_id'    => $post_data['lead'],
                        'purpose_id' => $post_data['purpose'],
                        'start_date' => $post_data['start_date'],
                        'end_date'   => $post_data['end_date'],
                        'remarks1'   => $post_data['remarks1']
                    );
        $result_check = $this->Calendar_model->checkVisitAvailability($dataArr);
        if($result_check)
        {
            $data['response'] = 'Visit has already been planed!';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        $this->db->trans_begin();
        $dataArr['created_by'] = $post_data['user_id'];
        $dataArr['created_time'] = date('Y-m-d H:i:s');
        $visit_id = $this->Common_model->insert_data('visit',$dataArr);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Visit has been planed successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }

    public function visit()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $_SESSION['company'] = $post_data['company_id'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $searchParams = array(
                'leadId'    => $post_data['lead_number'],
                'customer'  => $post_data['customer'],
                'startDate' => $post_data['startDate'],
                'endDate'   => $post_data['endDate']
            );
        $current_offset = ($post_data['segment']!='')?$post_data['segment']:0;
        $config['per_page'] = getDefaultPerPageRecords(); 
        $data['visitSearch'] = $this->Calendar_model->visitResults($searchParams, $config['per_page'], $current_offset);
        $this->session->sess_destroy();
        echo json_encode($data);

    }

    public function editvisit()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $visit_id = $post_data['visit_id'];
        $where = array('visit_id' => $visit_id);

        $data['visitEdit'] = $this->Common_model->get_data_row('visit', $where);
        $data['customer'] = $this->Calendar_model->get_lead_customer($data['visitEdit']['lead_id']);
        $data['purpose'] = $this->Common_model->get_data('visit_purpose',array());
        echo json_encode($data);
    }

    public function updatevisit()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $visit_id = $post_data['visit_id'];
        $lead_id = $post_data['lead_id'];
        $dataArr = array
                    (
                        'lead_id'    => $lead_id,
                        'visit_id'   => $visit_id,
                        'purpose_id' => $post_data['purpose'],
                        'start_date' => $post_data['start_date'],
                        'end_date'   => $post_data['end_date'],
                        'remarks1'   => $post_data['remarks1']
                    );
        $result_check = $this->Calendar_model->checkVisitAvailability($dataArr);
        if($result_check)
        {
            $data['response'] = 'Visit has already been planed!';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        $this->db->trans_begin();
        $dataArr['modified_by'] = $post_data['user_id'];
        $dataArr['modified_time'] = date('Y-m-d H:i:s');
        $where = array('visit_id' => $visit_id);
        $this->Common_model->update_data('visit',$dataArr, $where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Visit has been Updated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }


    }

    public function getreportees()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $_SESSION['reportees'] = $post_data['reportees'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $_SESSION['company'] = $post_data['company_id'];
        $val = @trim($post_data['name']);
        $level = 0;
        $data = $this->ajax_model->getReporteesWithUser($val, $level);
        $this->session->sess_destroy();
        echo json_encode($data);
    }


    public function viewCalendar()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $user_id = $post_data['user_id'];
        $reportees = $post_data['reportees'];
        if($reportees!='')
        {
            $where = $reportees;
        }
        else
        {
            $where = $user_id;
        }
        $data['visitCalendarDetails'] = $this->Calendar_model->visitCalendarDetails($where);
        $data['demoCalendarDetails'] = $this->Calendar_model->demoCalendarDetails($where);
        echo json_encode($data);
    }

    public function deleteVisit()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $visit_id = $post_data['visit_id'];
        $where = array('visit_id' => $visit_id);
        $dataArr = array('status' => 2);
        $this->db->trans_begin();
        $this->Common_model->update_data('visit',$dataArr, $where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Visit has been De-Activated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }


    public function activateVisit()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $visit_id = $post_data['visit_id'];
        $where = array('visit_id' => $visit_id);
        $results = $this->Common_model->get_data_row('visit', $where);
        $data_res = array('visit_id' => '','lead_id'=>$results['lead_id'],'start_date'=>$results['start_date'],'end_date'=>$results['end_date']);
        $result_check = $this->Calendar_model->checkVisitAvailability($data_res);
        if($result_check)
        {
            $data['response'] = 'Visit has already been planed!';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        $dataArr = array('status' => 1);
        $this->db->trans_begin();
        $this->Common_model->update_data('visit',$dataArr, $where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Visit has been Activated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }


    public function update_visitFeedback()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $visit_id = $post_data['visit_id'];
        $where = array('visit_id'=>$visit_id);
        $data_arr = array('remarks2'      => $post_data['remarks2'],
                      'modified_by'   => $post_data['user_id'],
                      'modified_time' => date('Y-m-d H:i:s'));

        $this->db->trans_begin();
        $this->Common_model->update_data('visit',$data_arr,$where);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Visit Feedback has been updated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }
    }

    public function addDemo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $_SESSION['company'] = $post_data['company_id'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $_SESSION['locationString'] = $post_data['locationString'];
        $_SESSION['role_id'] = $post_data['role_id'];
        $leads = $this->Calendar_model->getLeadDetails(1);
        $lead_arr = array();
        foreach ($leads as $key=>$lead) 
        {
            $lead_arr[$key]['lead_id'] = $lead['lead_id'];
            $lead_arr[$key]['name'] = "Lead ID - ".$lead['lead_number']." (".$lead['CustomerName'].")";
        }
        $data['leads'] = $lead_arr;
        $this->session->sess_destroy();
        echo json_encode($data); 

    }

    public function getOpportunity()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $lead_id = $post_data['lead_id'];
        $_SESSION['company'] = $post_data['company_id'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $results = $this->Calendar_model->getOpportunity($lead_id);
        $opportunities = array();
        foreach ($results as $key=>$value) 
        {
            $opportunities[]= array('opportunity_id'=>$key,'name'=>$value);
        }
        $this->session->sess_destroy();
        echo json_encode($opportunities);
    }

    public function getDemo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $opportunity_id = @$post_data['opportunity_id'];
        $results = $this->Calendar_model->getDemo($opportunity_id);
        $demo = array();
        foreach ($results as $key=>$value) 
        {
            $demo[]= array('demo_id'=>$key,'name'=>$value);
        }
        $this->session->sess_destroy();
        echo json_encode($demo);
    }

    public function demoAdd()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $opportunity_id = $post_data['opportunity_id'];
        $product_id = $this->Common_model->get_value('opportunity_product', array('opportunity_id'=>$opportunity_id), 'product_id');
        $demo_id = '';
        $dataArr = array
                    (
                        'demo_id'         => $demo_id,
                        'opportunity_id'  => $opportunity_id,
                        'product_id'      => $product_id,
                        'demo_product_id' => $post_data['demo_machine_id'],
                        'start_date'      => $post_data['start_date'],
                        'end_date'        => $post_data['end_date'],
                        'remarks1'        => $post_data['remarks1']
                    );
        $result_check = $this->Calendar_model->checkDemoAvailability($dataArr);
        if($result_check)
        {
            $data['response'] = 'Demo has already been booked!';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        $this->db->trans_begin();
        $dataArr['created_by'] = $post_data['user_id'];
        $dataArr['created_time'] = date('Y-m-d H:i:s');
        $this->Common_model->insert_data('demo',$dataArr);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Demo has been planed successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }

    public function demo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $_SESSION['company'] = $post_data['company_id'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $searchParams = array(
                'opportunityId' => $post_data['opportunity_number'],
                'customer'      => $post_data['customer'],
                'startDate'     => $post_data['startDate'],
                'endDate'       => $post_data['endDate']
            );
        $current_offset = ($post_data['segment']!='')?$post_data['segment']:0;
        $config['per_page'] = getDefaultPerPageRecords(); 
        $data['demoSearch'] = $this->Calendar_model->demoResults($searchParams, $config['per_page'], $current_offset);
        $this->session->sess_destroy();
        echo json_encode($data);

    }

    public function editdemo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $demo_id = $post_data['demo_id'];
        $opportunity_id = $post_data['opportunity_id'];
        $where = array('demo_id' => $demo_id);
        $_SESSION['company'] = $post_data['company_id'];
        $_SESSION['user_id'] = $post_data['user_id'];
        $data['demoEdit'] = $this->Common_model->get_data_row('demo', $where);
        $lead_id = $this->Common_model->get_value('opportunity',array('opportunity_id'=>$opportunity_id),'lead_id');
        $data['customer'] = $this->Calendar_model->get_lead_customer($lead_id);
        $data['opportunity'] = $this->Calendar_model->getOpportunity_for_edit_demo($lead_id,$opportunity_id);
        $demo_machine = $this->Calendar_model->getDemoname_for_api($data['demoEdit']['demo_product_id']);
        $data['opportunity']['demo_machine'] = $demo_machine['demoProduct'];
        $this->session->sess_destroy();
        echo json_encode($data);
    }

    public function updatedemo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $demo_id = $post_data['demo_id'];
        $opportunity_id = $post_data['opportunity_id'];
        $product_id = $this->Common_model->get_value('opportunity_product', array('opportunity_id'=>$opportunity_id), 'product_id');
        $dataArr = array
                    (
                        'demo_id'         => $demo_id,
                        'opportunity_id'  => $opportunity_id,
                        'product_id'      => $product_id,
                        'demo_product_id' => $post_data['demo_machine_id'],
                        'start_date'      => $post_data['start_date'],
                        'end_date'        => $post_data['end_date'],
                        'remarks1'        => $post_data['remarks1']
                    );
        $result_check = $this->Calendar_model->checkDemoAvailability($dataArr);
        if($result_check)
        {
            $data['response'] = 'Demo has already been planed!';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        $this->db->trans_begin();
        $dataArr['modified_by'] = $post_data['user_id'];
        $dataArr['modified_time'] = date('Y-m-d H:i:s');
        $where = array('demo_id' => $demo_id);
        $this->Common_model->update_data('demo',$dataArr, $where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Demo has been updated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }

    public function deleteDemo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $demo_id = $post_data['demo_id'];
        $where = array('demo_id' => $demo_id);
        $dataArr = array('status' => 2);
        $this->db->trans_begin();
        $this->Common_model->update_data('demo',$dataArr, $where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Demo has been De-Activated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }


    public function activateDemo()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $demo_id = $post_data['demo_id'];
        $where = array('demo_id' => $demo_id);
        $results = $this->Common_model->get_data_row('demo', $where);
        $data_res = array('demo_id' => '','demo_product_id'=>$results['demo_product_id'],'start_date'=>$results['start_date'],'end_date'=>$results['end_date']);
        $result_check = $this->Calendar_model->checkDemoAvailability($data_res);
        if($result_check)
        {
            $data['response'] = 'Demo has already been planed!';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        $dataArr = array('status' => 1);
        $this->db->trans_begin();
        $this->Common_model->update_data('demo',$dataArr, $where);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Demo has been Activated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }

    }


    public function update_demoFeedback()
    {
        $json = file_get_contents('php://input');
        $post_data = json_decode($json,TRUE);
        $demo_id = $post_data['demo_id'];
        $where = array('demo_id'=>$demo_id);
        $data_arr = array('remarks2'      => $post_data['remarks2'],
                      'modified_by'   => $post_data['user_id'],
                      'modified_time' => date('Y-m-d H:i:s'));

        $this->db->trans_begin();
        $this->Common_model->update_data('demo',$data_arr,$where);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $data['response'] = 'Something Went Wrong';
            echo json_encode($data);
            header("Status: 400 Bad Request",true,400); exit;
        }
        else
        {
            $this->db->trans_commit();
            $data['response'] = 'Demo Feedback has been updated successfully!';
            echo json_encode($data);
            header("HTTP/1.1 201 Created"); exit;
        }
    }

}