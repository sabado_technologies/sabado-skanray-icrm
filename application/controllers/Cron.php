<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Base_controller.php';


class Cron extends CI_controller {

	public function __construct() 
	{
        parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model('Common_model');
		
	}

	public function cron_daily($print_mail=0){

		//send_email('navee.naveen@gmail.com','Testing-Daily Cron','cron test - daily cron'.date('Y m d H:i:s'));


		define('PRINT_MAIL',$print_mail);

		reminder_orderConclusionDatePast();
		escalation1_orderConclusionDatePast();
		escalation2_orderConclusionDatePast();

		reminder_postDemoNotUpdated();
		escalation1_postDemoNotUpdated();
		escalation2_postDemoNotUpdated();

		reminder_postVisitNotUpdated();
		escalation1_postVisitNotUpdated();
		escalation2_postVisitNotUpdated();
	}

	public function cron_5minute($print_mail=0){

		define('PRINT_MAIL',$print_mail);

		//send_email('navee.naveen@gmail.com','Testing-5 minute Cron','cron test - 5 minute cron'.date('Y m d H:i:s'));

		if(date('H') == 7 && date('i') >= 0 && date('i') < 5) 
		{
			//send_email('navee.naveen@gmail.com','Testing-Before daily cron','cron test - before daily cron'.date('Y m d H:i:s'));
			$this->cron_daily();
		}
		notification_leadApproval();
		notification_leadAssigned();
	}

	public function cron_test()
	{
		send_email('navee.naveen@gmail.com','Test','cron test'.date('Y m d H:i:s'));
	}

	public function cron_test1()
	{
		send_email('navee.naveen@gmail.com','Testing-direct cron','cron test - direct'.date('Y m d H:i:s'));
	}
	
	/*punchout at the end of the day , if user is not punch out */
	public function force_punch_out()
    {
        $data = array('end_time'     =>    date('Y-m-d H:i:s'),
                     'modified_by'   =>    '1',
                     'modified_time' =>    date('Y-m-d H:i:s')
                    );
        $where = array('end_time'=> '');
        $this->Common_model->update_data('punch_in',$data,array('end_time is null'));
	}
	/*
	Deleting live location data until previous month
	*/
    public function delete_live_loc_records()
    {
        $delete_date = date('Y-m-d', strtotime(date('Y-m-01').' -1 MONTH'));
        $sql = 'delete from live_location where created_date <"'.$delete_date.'"';
        $this->db->query($sql);

    }

	
}