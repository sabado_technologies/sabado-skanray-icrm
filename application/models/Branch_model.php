<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branch_model extends CI_Model {

    public function branchDetails($searchParams) {

        $this->db->select('*');
        $this->db->from('branch');
        if (@$searchParams['branchName'] != '')
            $this->db->like('name', @$searchParams['branchName']);
        $this->db->where('company_id',$this->session->userdata('company'));
        $this->db->order_by('name');
        $res = $this->db->get();

        $data = $res->result_array();
        return $data;
    }

    public function branchResults($searchParams, $per_page, $current_offset) {

        $this->db->select('branch_id,name,status');
        $this->db->from('branch');
        if (@$searchParams['branchName'] != '')
            $this->db->like('name', @$searchParams['branchName']);
        $this->db->where('company_id',$this->session->userdata('company'));
        $this->db->limit($per_page, $current_offset);
        $this->db->order_by('branch_id', 'DESC');
        $res = $this->db->get();

        $data = $res->result_array();
        return $data;
    }

    public function branchTotalRows($searchParams) {

        $this->db->from('branch');
        if (@$searchParams['branchName'] != '')
            $this->db->like('name', @$searchParams['branchName']);
        $this->db->where('company_id',$this->session->userdata('company'));
        $res = $this->db->get();
        return $res->num_rows();
    }

    function is_branchNameExist($branch_name,$branch_id)
    {
        //echo $branch_id; exit;
        $this->db->select();
        $this->db->where('name',$branch_name);  
                if($branch_id!='')
        $this->db->where('branch_id !=',$branch_id);
        $this->db->where('company_id',$this->session->userdata('company'));
        $query = $this->db->get('branch');
        return ($query->num_rows()>0)?1:0;
    }

    public function get_SE_users()
    {
        $sql = "SELECT u.user_id,pi.punch_in_id, pi.latitude, pi.longitude, concat(u.first_name, ' _ ', u.employee_id) as user_name FROM user u JOIN punch_in pi ON pi.user_id=u.user_id WHERE u.company_id = '".$this->session->userdata('company')."' AND u.status=1 AND pi.created_time IN (SELECT MAX( p.created_time ) FROM punch_in p WHERE p.user_id = pi.user_id ) GROUP BY pi.user_id ORDER BY u.user_id DESC";
        $res = $this->db->query($sql);
        return $res->result_array();
    }
    
    public function check_punch_in($user_id)
    {
        $this->db->select('punch_in_id');
        $this->db->from('punch_in');
        $this->db->where('user_id',$user_id);
        $this->db->where('end_time IS NULL');
        $this->db->where('DATE(start_time)',date('Y-m-d'));
        $qry = $this->db->get();
        return $qry->row_array();
    }

}
